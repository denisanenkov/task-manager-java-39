package ru.anenkov.tm.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.entiity.Task;

import java.util.List;

@Component
public class TaskCommunication {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL = "http://localhost:8080/api";

    public List<TaskDTO> tasks() {
        ResponseEntity<List<TaskDTO>> responseEntity =
                restTemplate.exchange(URL + "/tasks", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
        List<TaskDTO> taskList = responseEntity.getBody();
        return taskList;
    }

    public TaskDTO getTask(String id) {
        TaskDTO task =
                restTemplate.getForObject(URL + "/task/" + id, TaskDTO.class);
        return task;
    }

    public void delete(String id) {
        restTemplate.delete(URL + "/task/" + id);
        System.out.println("Task with id \"" + id + "\" was deleted");
    }

    public void addTask(Task task) {
        if (!isExists(task.getId())) {
            ResponseEntity<String> responseEntity =
                    restTemplate.postForEntity(URL + "/task", task, String.class);
            System.out.println("Task was added!");
            System.out.println(responseEntity.getBody());
        } else {
            restTemplate.put(URL + "/task", task);
            System.out.println("Task with id \"" + task.getId() + "\" was UPDATED!");
        }
    }

    public boolean isExists(String id) {
        List<TaskDTO> taskList = tasks();
        for (TaskDTO currentTask : taskList) {
            if (id.equals(currentTask.getId())) return true;
        }
        return false;
    }

}
