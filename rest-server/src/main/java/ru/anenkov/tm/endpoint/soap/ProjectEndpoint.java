package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.service.ProjectDTOService;
import ru.anenkov.tm.dto.ProjectDTO;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectDTOService projectDTOService;

    @WebMethod
    public void add(@WebParam(name = "project") ProjectDTO project) {
        projectDTOService.add(project);
    }

    @WebMethod
    public ProjectDTO findOneByIdEntity(@WebParam(name = "id") String id) {
        return projectDTOService.findOneByIdEntity(id);
    }

    @WebMethod
    public void removeOneById(@WebParam(name = "id") String id) {
        projectDTOService.removeOneById(id);
    }

    @WebMethod
    public void removeAllProjects() {
        projectDTOService.removeAll();
    }

    @WebMethod
    public List<ProjectDTO> getList() {
        return projectDTOService.getList();
    }

    @WebMethod
    public long count() {
        return projectDTOService.count();
    }

}
