package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.service.TaskDTOService;
import ru.anenkov.tm.dto.TaskDTO;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    TaskDTOService taskDTOService;

    @WebMethod
    public void add(@WebParam(name = "task") TaskDTO task) {
        taskDTOService.add(task);
    }

    @WebMethod
    public TaskDTO findOneByIdEntity(@WebParam(name = "id") String id) {
        return taskDTOService.findOneByIdEntity(id);
    }

    @WebMethod
    public void removeOneById(@WebParam(name = "id") String id) {
        taskDTOService.removeOneById(id);
    }

    @WebMethod
    public List<TaskDTO> getList() {
        return taskDTOService.getList();
    }

    @WebMethod
    public void removeAll() {
        taskDTOService.removeAll();
    }

    @WebMethod
    public long count() {
        return taskDTOService.count();
    }

}
