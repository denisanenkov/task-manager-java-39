package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.api.endpoint.ITaskRestEndpoint;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @Override
    @GetMapping("/tasks")
    public List<Task> tasks() {
        return taskService.getList();
    }

    @Override
    @GetMapping("/task/{id}")
    public Task getTask(@PathVariable String id) {
        Task task = taskService.findOneByIdEntity(id);
        System.out.println("FINDED: " + task);
        if (task == null) throw new NoSuchEntitiesException
                ("Entity \"Task\" with id = " + id + " not found!");
        return task;
    }

    @Override
    @PostMapping("/task")
    public Task addTask(@RequestBody Task task) {
        taskService.add(task);
        return task;
    }

    @Override
    @PutMapping("/task")
    public Task updateTask(@RequestBody Task task) {
        taskService.add(task);
        return task;
    }

    @Override
    @DeleteMapping("/task/{id}")
    public String deleteTask(@PathVariable String id) {
        taskService.removeOneById(id);
        return "Task with id " + id + " was deleted successfully!";
    }

    @Override
    @DeleteMapping("/tasks")
    public void deleteAllTasks() {
        taskService.removeAllTasks();
    }

    @Override
    @GetMapping("/tasks/count")
    public long count() {
        return taskService.count();
    }

}
