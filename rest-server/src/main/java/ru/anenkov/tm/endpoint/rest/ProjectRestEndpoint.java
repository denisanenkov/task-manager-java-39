package ru.anenkov.tm.endpoint.rest;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.api.endpoint.IProjectRestEndpoint;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.service.ProjectDTOService;
import ru.anenkov.tm.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    ProjectService projectService;

    @Autowired
    ProjectDTOService projectDTOService;

    @Override
    @PostMapping("/project")
    public Project add(@Nullable @RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @PutMapping("/project")
    public Project update(@Nullable @RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @GetMapping("/project/{id}")
    public @Nullable ProjectDTO findOneByIdEntity(@PathVariable("id") String id) {
        return projectDTOService.findOneByIdEntity(id);
    }

    @Override
    @DeleteMapping("/project/{id}")
    public String removeOneById(@PathVariable String id) {
        Project project = projectService.findOneByIdEntity(id);
        if (project == null)
            throw new NoSuchEntitiesException("project with id \"" + id + "\" does not exist!");
        projectService.removeOneById(id);
        return "Project with id \"" + id + "\" was deleted successfully!";
    }

    @Override
    @DeleteMapping("/projects")
    public void removeAllProjects() {
        projectService.removeAllProjects();
    }

    @Override
    @Nullable
    @GetMapping("/projects")
    public List<ProjectDTO> getList() {
        return projectDTOService.getList();
    }

    @Override
    @GetMapping("/projects/count")
    public long count() {
        return projectService.count();
    }

}
