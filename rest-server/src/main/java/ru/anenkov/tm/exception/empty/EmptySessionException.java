package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptySessionException extends AbstractException {

    public EmptySessionException() {
        System.out.println("Error! Session is null..");
    }

}
