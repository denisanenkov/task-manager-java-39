package ru.anenkov.tm.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "task")
public class TaskDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    private String description = "";

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "TaskDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", dateBegin=" + dateBegin +
                ", dateFinish=" + dateFinish +
                ", projectId='" + projectId + '\'' +
                '}';
    }

}
