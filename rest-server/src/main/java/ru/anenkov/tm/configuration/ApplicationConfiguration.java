package ru.anenkov.tm.configuration;

import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.context.annotation.Bean;
import ru.anenkov.tm.endpoint.soap.ProjectEndpoint;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.anenkov.tm.endpoint.soap.TaskEndpoint;

import javax.xml.ws.Endpoint;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class ApplicationConfiguration implements WebApplicationInitializer {

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint dateEndpointRegistry(final ProjectEndpoint projectEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}
