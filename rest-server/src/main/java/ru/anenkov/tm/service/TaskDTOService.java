package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.repository.TaskDTORepository;

import java.util.List;

@Service
public class TaskDTOService {

    @Nullable
    @Autowired
    private TaskDTORepository taskRepository;

    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final TaskDTO task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<TaskDTO> getList() {
        return taskRepository.findAll();
    }

    @SneakyThrows
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return taskRepository.count();
    }

}
