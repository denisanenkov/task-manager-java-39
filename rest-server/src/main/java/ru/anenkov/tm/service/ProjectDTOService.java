package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.repository.ProjectDTORepository;

import java.util.List;

@Service
public class ProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository projectDTORepository;

    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final ProjectDTO project
    ) {
        if (project == null) throw new EmptyEntityException();
        projectDTORepository.save(project);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public ProjectDTO findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectDTORepository.findById(id).orElse(null);
    }

    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectDTORepository.deleteById(id);
    }

    public void removeAll() {
        projectDTORepository.deleteAll();
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<ProjectDTO> getList() {
        return projectDTORepository.findAll();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return projectDTORepository.count();
    }

}
