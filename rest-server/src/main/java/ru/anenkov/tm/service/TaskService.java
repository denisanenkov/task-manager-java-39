package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Task> getList() {
        return taskRepository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllTasks() {
        taskRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return taskRepository.count();
    }

}
