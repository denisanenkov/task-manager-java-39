package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    Project findOneByIdEntity(@Nullable String id);

    void removeOneById(@Nullable String id);

    void removeAllProjects();

    void add(@Nullable Project project);

    @Nullable
    List<Project> getList();

    long count();

}