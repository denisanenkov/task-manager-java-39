package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    @Transactional(readOnly = true)
    Task findTaskById(@Nullable @Param("id") final String id);

    @Transactional
    void removeById(@NotNull @Param("id") final String id);

}
